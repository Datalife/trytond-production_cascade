# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import production
from . import product


def register():
    Pool.register(
        production.Production,
        product.Product,
        module='production_cascade', type_='model')
