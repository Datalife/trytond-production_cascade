# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta

__all__ = ['Product']


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    def _get_cascade_inputs(self, deep=0):
        res = {}
        if not self.producible or not self.boms:
            return res
        res[deep] = [i.product for i in self.boms[0].bom.inputs]
        for item in res[deep]:
            res.setdefault(deep + 1, [])
            inputs = item._get_cascade_inputs(deep + 1)
            if inputs:
                res[deep + 1].extend(inputs[deep + 1])
        return res
